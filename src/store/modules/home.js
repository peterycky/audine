export default {
  state: {
    module2: {
      home: {
        titleUpper: 'Bez wychodzenia',
        titleLower: 'z domu',
        paragraph: 'Nie musisz tracić czasu na dojazd, stanie w korkach czy w kolejce. Cały test wykonasz samodzielnie w zaciszu swojego domu.',
        avatar: 'dom.svg'
      },
      data: {
        titleUpper: 'Nie zbieramy o Tobie',
        titleLower: 'żadnych danych',
        paragraph: 'Nie zbieramy żadnych Twoich danych. Te, które wygenerujesz, wykonując test, zostają  w pamięci używanej przez Ciebie przeglądarki. Dane zapisane w przeglądarce przetwarzamy wyłącznie po to, aby zapewnić funkcjonalność testu.',
        avatar: 'dane.svg'
      },
      visibility: {
        titleUpper: 'Wyniki są widoczne tylko',
        titleLower: 'dla Ciebie',
        paragraph: 'Nikt prócz Ciebie nie ma dostępu do wyników testu, dopóki ich komuś nie udostępnisz. Zostaną one zapisane w pamięci przeglądarki, nawet po zamknięciu aplikacji, do czasu usunięcia plików cookies.',
        avatar: 'lupa.svg'
      },
      doctore: {
        titleUpper: 'Najbardziej zbliżony do',
        titleLower: 'badania audiologa',
        paragraph: 'Jest to jedyny test słuchu dostępny w sieci, który działa w oparciu o audiogram tonowo-szumowy. Nie zastępuje wizyty u specjalisty, jednak może pomóc wstępnie ustalić stan narządu słuchu oraz wykazać nieprawidłowości w jego działaniu.',
        avatar: 'doktorek.svg'
      },
      science: {
        titleUpper: 'Napisany jako projekt',
        titleLower: 'naukowy',
        paragraph: 'Wszystkie elementy strony internetowej, na której teraz jesteś, stanowią wynik pracy naukowej napisanej jako projekt dyplomowy przez studenta Akademii Górniczo-Hutniczej. Spokojnie, nikt tutaj nie próbuje ukraść Twoich danych albo czegokolwiek Ci sprzedać.',
        avatar: 'mikroskop.svg'
      },
      time: {
        titleUpper: 'Zajmuje',
        titleLower: 'od 5 do 20 minut',
        paragraph: 'Szanujemy Twój czas. Wykonanie testu nie powinno trwać długo, ale jego całkowity czas zależy od stanu Twojego słuchu oraz przygotowanego stanowiska pomiarowego.',
        avatar: 'czas.svg'
      },
      money: {
        titleUpper: 'Całkowicie',
        titleLower: 'darmowy',
        paragraph: 'Udział w teście nic nie kosztuje, ponieważ jest to projekt dyplomowy, który nie został sfinansowany przez żadne firmy czy instytucje.',
        avatar: 'hajs.svg'
      }
    },
    module3: {
      step: 1,
      data: {
        firstStep: {
          imgSrc: 'teststep1.png',
          title: 'Ustawienie głośności',
          paragraph: 'Wyłącz dźwięk w aplikacjach, które mogą zakłucić przeprowadzenie testu (komunikatory, aplikacje systemowe itp.). Następnie ustaw odpowiedni poziom głośności dla swoich słuchawek– powinny one dostarczać dźwięki intensywne, o maksymalnej wartości, jaka jeszcze będzie dla Ciebie komfortowa.'
        },
        secondStep: {
          imgSrc: 'teststep2.png',
          title: 'Seria prób',
          paragraph: 'Usłyszysz dźwięki o różnych częstotliwościach – jedne piszcące, inne buczące. Gdy tylko wychwycisz chociażby najcichszy dźwięk, natychmiast wciśnij wskazany przycisk. Wszystkie próby wykonuj według instrukcji podawanej na biążąco w trakcie trwawnia testu.'
        },
        thirdStep: {
          imgSrc: 'teststep3.png',
          title: 'Przedstawienie wyników',
          paragraph: 'Po skończonej serii prób zobaczysz wyniki testu przedstawione w formie wykresu tonowo-szumowego, który będziesz mógł wydrukować. Wyniki można także przedstawieć w tabeli, np. w arkuszu Exela.'
        }
      }
    },
    module4: {
      headphones: {
        titleUpper: 'Sprawne słuchawki',
        paragraph: 'Nie potrzebujesz drogiego i profesjonalnego sprzętu. Wystarczą zwykłe słuchawki, których używasz np. do słuchania muzyki. Ważne, by działały w trybie stereo, tzn. z rozdziałem transmisji dźwięku na dwa kanały – prawy i lewy. Jeśli nie masz pewności, sprawdź przed testem, czy na pewno wszystko działa prawidłowo.',
        avatar: 'headphones.svg'
      },
      age: {
        titleUpper: 'Minimalny wiek',
        paragraph: 'Minimalny  sugerowany wiek uczestnika testu to 13 lat. Często zdaża się, że dzieci we wcześniejszych latach przechodzą choroby, których objawów nie widać. Wynika to z fizjologii i samego procesu dojrzewania. Dlatego wyniki testu prawdopodobnie byłyby niemiarodajne. Dziecko najlepiej umówić na wizytę u audiologa.',
        avatar: 'age.svg'
      },
      goodhealth: {
        titleUpper: 'Dobre zdrowie',
        paragraph: 'Wszelkie choroby i infekcje pogarszające pracę narządu słuchu (grypa, przeziębienie, zapalenie ucha, zatkane uszy itp.) spowodują przekłamanie wyników. Dlatego też upewnij się, czy Twój stan zdrowia pozwala na udziału w teście.',
        avatar: 'health.svg'
      }
    },
    module5: {
      test: {
        title: 'Chcę rozpocząć test',
        link: '/test'
      },
      simulation: {
        title: 'Chcę rozpocząć symulację',
        link: '/simulation'
      }
    }
  },

  mutations: {
    homeM3stepSet: (state, myStep = 1) => {
      state.module3.step = myStep
    },
    homeM3stepInc: state => {
      if (state.module3.step <= 2) {
        state.module3.step += 1
      } else {
        // state.module.step = 3
      }
    },
    homeM3stepDec: state => {
      if (state.module3.step > 1) {
        state.module3.step -= 1
      } else {
        // state.module.step = 1
      }
    }
  },

  actions: {
    changeStepHome: (context, myStep) => {
      context.commit('homeM3stepSet', myStep)
    },
    incStepHome: context => {
      context.commit('homeM3stepInc')
    },
    decStepHome: context => {
      context.commit('homeM3stepDec')
    }
  },

  getters: {
    getHomeUpperCardInfo (state) {
      return state.module2
    },
    getHomeCarouselInfo (state) {
      return state.module3
    },
    getHomeSecondaryCardInfo (state) {
      return state.module4
    },
    getButtontest (state) {
      return state.module5
    }
  }
}
