
export default {
  state: {
    currentStep: 0, // TODO: default from START!
    // currentStep: 6, // *currently for debugging purposes, start from 4th view
    phases: [
      {
        // Step 0 - Headphones reminder
        controlCircleData: {
          icon: 'HeadphonesWhite.svg',
          borderAnimation: false,
          focusAnimation: false,
          clickable: false
        },
        narratorData: [
          'Załóż słuchawki i kliknij <b>DALEJ</b>',
          'lub naciśnij <b>SPACJĘ</b>'
        ],
        buttonVisibility: true
      },
      {
        // Step 1 - Notifications reminder
        controlCircleData: {
          icon: 'Notification.svg',
          borderAnimation: false,
          focusAnimation: false,
          clickable: false
        },
        narratorData: [
          'Wyłącz wszystkie powiadomienia, zakładki',
          'oraz aplikacje, które generują dźwięki',
          'i kliknij <b>DALEJ</b>'
        ],
        buttonVisibility: true
      },
      {
        // Step 2 - Turn on configuration audio
        controlCircleData: {
          icon: 'Play.svg',
          borderAnimation: false,
          focusAnimation: true,
          clickable: true
        },
        narratorData: ['Włącz ścieżkę dźwiękową'],
        buttonVisibility: false
      },
      {
        // Step 3 - Audio configuration screen
        controlCircleData: {
          icon: 'dots',
          borderAnimation: false,
          focusAnimation: false,
          clickable: false
        },
        narratorData: [
          'Ustaw na swoim urządzeniu <b>maksymalny komfortowy</b>',
          'poziom głośności i kliknij <b>DALEJ</b>'
        ],
        buttonVisibility: true
      },
      {
        // Step 4 - Start Test screen
        controlCircleData: {
          icon: 'Play.svg',
          borderAnimation: false,
          focusAnimation: true,
          clickable: true
        },
        narratorData: ['<b>Kliknij przycisk, by rozpocząć test</b>'],
        buttonVisibility: false
      },
      {
        // Step 5 - During test screen
        controlCircleData: {
          icon: 'HeadphonesColour.svg',
          borderAnimation: true,
          focusAnimation: false,
          clickable: true
        },
        narratorData: [
          'Gdy tylko usłyszysz dźwięk,',
          'naciśnij <b>słuchawki</b> na ekranie lub <b>spację</b> na klawiaturze'
        ],
        buttonVisibility: false
      },
      {
        // Step 6 - Test completed
        controlCircleData: {
          icon: 'DoubleTick.svg',
          borderAnimation: false,
          focusAnimation: false,
          clickable: false
        },
        narratorData: [
          'Wszystkie próby zakończyły się pomyślnie,',
          'Kliknij <b>DALEJ</b>, by przejść do wyników'
        ],
        buttonVisibility: true
      },
      {
        // Step 7 - Results
      }
    ],
    audioComponent: {
      allowPlay: false,
      interrupt: false
    },
    audioResults: {
      // Mocked values for testing purposes, overwritten after test
      L: {
        '250': -10,
        '500': -20,
        '1000': -30,
        '2000': -20,
        '3000': -15,
        '4000': -10,
        '6000': 0,
        '8000': 0
      },
      R: {
        '250': 0,
        '500': 0,
        '1000': 0,
        '2000': -20,
        '3000': -10,
        '4000': -10,
        '6000': -25,
        '8000': -35
      }
    }
  },

  mutations: {
    testChangeStep (state, newStep) {
      state.currentStep = newStep
    },
    testPhase5TickTemporary (state, stance = 'off') {
      if (stance === 'on') {
        state.phases[5].controlCircleData.icon = 'Tick.svg'
        state.phases[5].controlCircleData.clickable = false
      } else {
        state.phases[5].controlCircleData.icon = 'HeadphonesColour.svg'
        setTimeout(() => {
          state.phases[5].controlCircleData.clickable = true
        }, 400)
      }
    },
    allowPlayAudio (state) {
      state.audioComponent.allowPlay = true
    },
    handleUserInterrupt (state) {
      state.audioComponent.interrupt = true
      setTimeout(() => {
        state.audioComponent.interrupt = false
      }, 200)
    },
    commitFinalResults (state, results) {
      state.audioResults = results
    }
  },

  actions: {
    incStepTest (context) {
      var step = context.getters.getTestStep
      step += 1
      context.commit('testChangeStep', step)
    },
    decStepTest (context) {
      var step = context.getters.getTestStep
      if (step > 1) {
        step -= 1
      }
      context.commit('testChangeStep', step)
    },
    changeTemporaryDuringTestIcon (context) {
      context.commit('testPhase5TickTemporary', 'on')
      setTimeout(() => {
        context.commit('testPhase5TickTemporary')
      }, 2000)
    },
    allowTestPlayAudio (context) {
      context.commit('allowPlayAudio')
    },
    userInterrupt (context) {
      // console.log('ACTION - User Interrupt')
      context.commit('handleUserInterrupt')
    },
    uploadFinalResults (context, results) {
      context.commit('commitFinalResults', results)
    }
  },

  getters: {
    getIfTestDuring (state) {
      if (state.currentStep < 7) {
        return true
      } else {
        return false
      }
    },
    getTestStep (state) {
      return state.currentStep
    },
    getTestUserStep (state) {
      if (state.currentStep <= 3) {
        return 1
      } else if (state.currentStep > 3 && state.currentStep < 7) {
        return 2
      } else {
        return 3
      }
    },
    getTestNarratorData (state) {
      return state.phases[state.currentStep]['narratorData']
    },
    checkTestNarratorSubtitles (state) {
      if (state.currentStep === 4) {
        return true
      } else {
        return false
      }
    },
    getTestProgressButtonVisibility (state) {
      return state.phases[state.currentStep]['buttonVisibility']
    },
    getTestControlCircleData (state) {
      return state.phases[state.currentStep]['controlCircleData']
    },
    getTestPlayAllow (state) {
      return state.audioComponent.allowPlay
    },
    getTestUserInterrupt (state) {
      return state.audioComponent.interrupt
    },
    getTestResults (state) {
      return state.audioResults
    }
  }
}
