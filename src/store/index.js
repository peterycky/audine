import Vue from 'vue'
import Vuex from 'vuex'

import home from './modules/home'
import test from './modules/test'
import simulation from './modules/simulation'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    home,
    test,
    simulation
  },

  state: {

  },

  getters: {

  },

  mutations: {

  },

  actions: {

  }
})
