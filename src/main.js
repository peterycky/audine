import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import Chartkick from 'vue-chartkick'
import Chart from 'chart.js'

import VueAudio from 'vue-audio-better'

Vue.use(Chartkick.use(Chart))
Vue.use(VueAudio)

Vue.config.productionTip = true

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
